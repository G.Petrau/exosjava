<html>
    <head>
        <title>Liste Films</title>
    </head>
    <body>
    <ul>
    <%
       ArrayList<Film> liste = (ArrayList<Film>) request.getAttribute("liste");

       for(Film film : liste)
       {

            String info = film.getTitre() + ",  " + film.getAnneeSortie() + "  ";

    %>
    <li><%= info%><a type='button' href="UpdateFilm?id=<%=film.getId%>">Modifier</a></li>
    <%
        }
    %>
    </body>
</html>