package Controller;
import DAO.FilmDAO;
import com.company.Film;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayFilm extends HttpServlet {
    public static final String VUE = "/WEB-INF/jsp/DisplayFilm.jsp";

    public DisplayFilm() {
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            FilmDAO filmDAO = new FilmDAO();
            new ArrayList();
            ArrayList<Film> listeFilm = filmDAO.findAll();
            req.setAttribute("liste", listeFilm);
        } catch (ClassNotFoundException | SQLException var5) {
            var5.printStackTrace();
        }

        this.getServletContext().getRequestDispatcher(VUE).forward(req, res);
    }
}
