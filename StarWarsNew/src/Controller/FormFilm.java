package Controller;

import DAO.FilmDAO;
import com.company.Film;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FormFilm extends HttpServlet {
    public static final String VUE = "/WEB-INF/jsp/FormFilm.jsp";

    public FormFilm() {
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher(VUE).forward(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int idFilm = Integer.parseInt(req.getParameter("idFilm"));
        String titre = req.getParameter("titre");
        int anneeSortie = Integer.parseInt(req.getParameter("dateDeSortie"));
        int numeroEpisode = Integer.parseInt(req.getParameter("numero"));
        int cout = Integer.parseInt(req.getParameter("cout"));
        int recette = Integer.parseInt(req.getParameter("recette"));

        try {
            FilmDAO fDAO = new FilmDAO();
            fDAO.create(new Film(idFilm, titre, anneeSortie, numeroEpisode, cout, recette));
            fDAO.fermerLaConnection();
        } catch (ClassNotFoundException | SQLException var10) {
            var10.printStackTrace();
        }

    }
}

