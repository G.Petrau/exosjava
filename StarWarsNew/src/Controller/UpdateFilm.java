package Controller;

import DAO.FilmDAO;
import com.company.Film;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UpdateFilm extends HttpServlet {
    public static final String VUE = "/WEB-INF/jsp/UpdateFilm.jsp";

    public UpdateFilm() {
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            FilmDAO fDAO = new FilmDAO();
            Film film = fDAO.find(id);
            req.setAttribute("film", film);
        } catch (ClassNotFoundException | SQLException var6) {
            var6.printStackTrace();
        }

        this.getServletContext().getRequestDispatcher(VUE).forward(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("idFilm"));
        String titre = req.getParameter("titre");
        int anneeSortie = Integer.parseInt(req.getParameter("dateDeSortie"));
        int numeroEpisode = Integer.parseInt(req.getParameter("numero"));
        int cout = Integer.parseInt(req.getParameter("cout"));
        int recette = Integer.parseInt(req.getParameter("recette"));

        try {
            FilmDAO fDAO = new FilmDAO();
            Film film = new Film(id, titre, anneeSortie, numeroEpisode, cout, recette);
            fDAO.update(film);
        } catch (ClassNotFoundException | SQLException var11) {
            var11.printStackTrace();
        }

    }
}
