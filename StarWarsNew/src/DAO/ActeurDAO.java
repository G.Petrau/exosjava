package DAO;

import com.company.Acteur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ActeurDAO extends DAO<Acteur> {
    public ActeurDAO() throws SQLException, ClassNotFoundException {
    }

    public boolean create(Acteur obj) {
        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("INSERT INTO acteur(id, nom, prenom) VALUES(?, ?, ?, ?, ?, ?)");
            st.setInt(1, obj.getId());
            st.setString(2, obj.getNom());
            st.setString(3, obj.getPrenom());
            int affectedRows = st.executeUpdate();
            return affectedRows != 0;
        } catch (SQLException var5) {
            var5.printStackTrace();
            return false;
        }
    }

    public boolean delete(Acteur obj) {
        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("DELETE FROM acteur WHERE id = ?");
            st.setInt(1, obj.getId());
            int affectedRows = st.executeUpdate();
            return affectedRows != 0;
        } catch (SQLException var5) {
            var5.printStackTrace();
            return false;
        }
    }

    public boolean update(Acteur obj) {
        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("UPDATE acteur SET nom = ?, prenom = ?WHERE id = ?", 1005, 1008);
            st.setString(1, obj.getNom());
            st.setString(2, obj.getPrenom());
            st.setInt(6, obj.getId());
            int affectedRows = st.executeUpdate();
            return affectedRows != 0;
        } catch (SQLException var5) {
            var5.printStackTrace();
            return false;
        }
    }

    public Acteur find(int id) {
        Acteur acteur = new Acteur();

        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("SELECT * FROM acteur WHERE id = ?", 1005, 1008);
            st.setInt(1, id);
            ResultSet result = st.executeQuery();
            if (result.first()) {
                acteur = new Acteur(result.getInt("id"), result.getString("nom"), result.getString("prenom"));
            }
        } catch (SQLException var6) {
            var6.printStackTrace();
        }

        return acteur;
    }

    public ArrayList<Acteur> findAll() {
        ArrayList liste = new ArrayList();

        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("SELECT * FROM acteur", 1005, 1008);
            ResultSet result = st.executeQuery();

            while(result.next()) {
                liste.add(new Acteur(result.getInt("id"), result.getString("nom"), result.getString("prenom")));
            }
        } catch (SQLException var5) {
            var5.printStackTrace();
        }

        return liste;
    }
}
