package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class DAO<T> {
    protected Connection connect = null;

    public DAO() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connect = DriverManager.getConnection("jdbc:mysql://localhost/edcstarwars?user=root&password=");
        } catch (ClassNotFoundException | SQLException var2) {
            var2.printStackTrace();
            throw var2;
        }
    }

    public abstract boolean create(T var1);

    public abstract boolean delete(T var1);

    public abstract boolean update(T var1);

    public abstract T find(int var1);

    public abstract ArrayList<T> findAll();
}
