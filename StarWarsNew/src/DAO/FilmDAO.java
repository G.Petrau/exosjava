package DAO;

import com.company.Acteur;
import com.company.Film;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class FilmDAO extends DAO<Film> {
    public FilmDAO() throws SQLException, ClassNotFoundException {
    }

    public boolean create(Film obj) {
        try {
            Connection conn = this.connect;
            ActeurDAO acteurDao = new ActeurDAO();
            PreparedStatement st = conn.prepareStatement("INSERT INTO film(id, titre, anneeSortie, numeroEpisode, recette, cout) VALUES(?, ?, ?, ?, ?, ?)");
            st.setInt(1, obj.getId());
            st.setString(2, obj.getTitre());
            st.setInt(3, obj.getAnneeSortie());
            st.setInt(4, obj.getNumeroEpisode());
            st.setInt(5, obj.getRecette());
            st.setInt(6, obj.getCout());
            int affectedRows = st.executeUpdate();
            Iterator var6 = obj.getListeActeurs().iterator();

            while(var6.hasNext()) {
                Acteur acteur = (Acteur)var6.next();
                acteurDao.create(acteur);
            }

            return affectedRows != 0;
        } catch (ClassNotFoundException | SQLException var8) {
            var8.printStackTrace();
            return false;
        }
    }

    public boolean delete(Film obj) {
        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("DELETE FROM film WHERE id = ?");
            PreparedStatement st2 = conn.prepareStatement("DELETE FROM filmacteur WHERE filmacteur.id = ?");
            st.setInt(1, obj.getId());
            st2.setInt(1, obj.getId());
            st2.executeUpdate();
            int affectedRows = st.executeUpdate();
            return affectedRows != 0;
        } catch (SQLException var6) {
            var6.printStackTrace();
            return false;
        }
    }

    public boolean update(Film obj) {
        try {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("UPDATE film SET titre = ?, anneeSortie = ?, numeroEpisode = ?, recette = ?, cout = ? WHERE id = ?");
            st.setString(1, obj.getTitre());
            st.setInt(2, obj.getAnneeSortie());
            st.setInt(3, obj.getNumeroEpisode());
            st.setInt(4, obj.getRecette());
            st.setInt(5, obj.getCout());
            st.setInt(6, obj.getId());
            int affectedRows = st.executeUpdate();
            return affectedRows != 0;
        } catch (SQLException var5) {
            var5.printStackTrace();
            return false;
        }
    }

    public Film find(int id) {
        Film film = new Film();

        try {
            Connection conn = this.connect;
            ActeurDAO acteurDao = new ActeurDAO();
            PreparedStatement st = conn.prepareStatement("SELECT * FROM film WHERE id = ?", 1005, 1008);
            st.setInt(1, id);
            ResultSet result = st.executeQuery();

            while(result.next()) {
                film = new Film(result.getInt("id"), result.getString("titre"), result.getInt("anneeSortie"), result.getInt("numeroEpisode"), result.getInt("recette"), result.getInt("cout"));
                PreparedStatement st2 = conn.prepareStatement("SELECT acteur.id FROM acteur INNER JOIN filmacteur ON acteur.id = filmacteur.idA INNER JOIN film ON film.id = filmacteur.idF WHERE film.id = ?");
                st2.setInt(1, film.getId());
                ResultSet result2 = st2.executeQuery();

                while(result2.next()) {
                    film.getListeActeurs().add(acteurDao.find(result2.getInt("id")));
                }
            }
        } catch (ClassNotFoundException | SQLException var9) {
            var9.printStackTrace();
        }

        return film;
    }

    public ArrayList<Film> findAll() {
        ArrayList liste = new ArrayList();

        try {
            Connection conn = this.connect;
            ActeurDAO acteurDao = new ActeurDAO();
            PreparedStatement st = conn.prepareStatement("SELECT * FROM film", 1005, 1008);
            ResultSet result = st.executeQuery();

            while(result.next()) {
                Film film = new Film(result.getInt("id"), result.getString("titre"), result.getInt("anneeSortie"), result.getInt("numeroEpisode"), result.getInt("recette"), result.getInt("cout"), result.getString("img"));
                PreparedStatement st2 = conn.prepareStatement("SELECT acteur.id FROM acteur INNER JOIN filmacteur ON acteur.id = filmacteur.idA INNER JOIN film ON film.id = filmacteur.idF WHERE film.id = ?");
                st2.setInt(1, film.getId());
                ResultSet result2 = st2.executeQuery();

                while(result2.next()) {
                    film.getListeActeurs().add(acteurDao.find(result2.getInt("id")));
                }

                liste.add(film);
            }
        } catch (ClassNotFoundException | SQLException var9) {
            var9.printStackTrace();
        }

        return liste;
    }

    public boolean ajouterImage(Film obj) throws SQLException {
        try{
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("UPDATE film SET img = ? WHERE id = ?");
            st.setString(1, obj.getImage());
            st.setInt(2, obj.getId());
            int affectedRows = st.executeUpdate();
            return affectedRows != 0;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void fermerLaConnection() throws SQLException {
        this.connect.close();
    }
}
