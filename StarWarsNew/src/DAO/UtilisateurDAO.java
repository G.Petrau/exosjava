package DAO;

import com.company.Utilisateur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UtilisateurDAO extends DAO<Utilisateur> {


    public UtilisateurDAO() throws SQLException, ClassNotFoundException {
        super();
    }

    @Override
    public boolean create(Utilisateur obj){
        return false;
    }

    public boolean insert(int id, String login, String mdp){
        try
        {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("INSERT INTO utilisateur(id, login, mdp) " +
                    "VALUES(?, ?, ?)");

            st.setInt(1, id);
            st.setString(2, login);
            st.setString(3, mdp);
            int affectedRows = st.executeUpdate();

            return affectedRows != 0;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Utilisateur obj)
    {
        try
        {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("DELETE FROM utilisateur WHERE id = ?");

            st.setInt(1, obj.getId());

            int affectedRows = st.executeUpdate();

            return affectedRows != 0;


        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean update(Utilisateur obj) {
        try
        {
            Connection conn = this.connect;
            PreparedStatement st = conn.prepareStatement("UPDATE utilisateur SET login = ?" +
                    "WHERE id = ?");
            st.setString(1, obj.getLogin());
            st.setInt(2, obj.getId());


            int affectedRows = st.executeUpdate();

            return affectedRows != 0;


        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Utilisateur find(int id) {
        Utilisateur utilisateur = new Utilisateur();
        try
        {

            Connection conn = this.connect;




            PreparedStatement st = conn.prepareStatement("SELECT * FROM utilisateur WHERE id = ?");
            st.setInt(1, id);

            ResultSet result = st.executeQuery();

            if(result.first())
            {
                utilisateur = new Utilisateur(result.getInt("id"),
                        result.getString("login"),
                        result.getString("mdp"));
            }
        }

        catch (SQLException e)
        {

            e.printStackTrace();

        }
        return utilisateur;
    }

    public Utilisateur findWithLogin(String login) {
        Utilisateur utilisateur = new Utilisateur();
        try
        {

            Connection conn = this.connect;




            PreparedStatement st = conn.prepareStatement("SELECT * FROM utilisateur WHERE login = ?");
            st.setString(1, login);

            ResultSet result = st.executeQuery();

            if(result.first())
            {
                utilisateur = new Utilisateur(result.getInt("id"),
                        result.getString("login"),
                        result.getString("mdp"));
            }
        }

        catch (SQLException e)
        {

            e.printStackTrace();

        }
        return utilisateur;
    }

    @Override
    public ArrayList<Utilisateur> findAll(){
        ArrayList<Utilisateur> liste = new ArrayList<>();

        try
        {
            Connection conn = this.connect;

            PreparedStatement st = conn.prepareStatement("SELECT * FROM utilisateur");
            ResultSet result = st.executeQuery();
            while (result.next())
            {
                liste.add(new Utilisateur(result.getInt("id"),
                        result.getString("login"),
                        result.getString("mdp")));
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return liste;
    }

    public Utilisateur CheckUser(String login, String mdp)
    {
        Utilisateur uti = null;
        try
        {
            Connection conn = this.connect;
            PreparedStatement st= conn.prepareStatement("SELECT * FROM utilisateur WHERE login = ? AND mdp = ?");
            st.setString(1,login);
            st.setString(2,mdp);
            ResultSet result = st.executeQuery();
            System.out.println(result.toString());
            if(result.first())
            {
                uti = new Utilisateur(result.getInt("id"),
                        result.getString("login"),
                        result.getString("mdp"));
                return uti;

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return uti;
    }





}

