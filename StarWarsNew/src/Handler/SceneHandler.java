package Handler;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;

import java.util.MissingResourceException;

public class SceneHandler extends Scene {

    private Parent enabledPane;
    private Parent disabledPane;

    public SceneHandler(Parent parent) {
        super(parent);
        this.setEnabledPane(parent);

    }

    public SceneHandler(Parent parent, double v, double v1) {
        super(parent, v, v1);
        this.setEnabledPane(parent);
    }

    public Parent getEnabledPane() {
        return enabledPane;
    }

    public void setEnabledPane(Parent enabledPane) {
        this.disabledPane = this.enabledPane;
        this.enabledPane = enabledPane;
        this.setRoot(enabledPane);
    }

    public Parent getDisabledPane() {
        return disabledPane;
    }

    public void swapPane()
    {
        if(this.disabledPane != null)
        {
            this.setEnabledPane(this.disabledPane);
        }
        else
        {
            throw new MissingResourceException("La scène n'est pas définit", "SceneHandler","enablePane");
        }
    }

}

