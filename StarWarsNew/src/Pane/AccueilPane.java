package Pane;

import com.company.Utilisateur;
import DAO.UtilisateurDAO;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import com.company.Main;

import java.io.InputStream;
import java.sql.SQLException;

public class AccueilPane extends BorderPane {

    private static String login;


    public AccueilPane(String login) throws SQLException, ClassNotFoundException {
        super();
        this.login = login;
        this.initPane();

    }

    public void initPane() throws SQLException, ClassNotFoundException {
        MenuBar menu = new MenuBar();
        MenuItem item1 = new MenuItem();
        MenuItem item2 = new MenuItem();
        Button deconnexion = new Button("Deconnexion");
        deconnexion.setStyle("-fx-font: 13 arial;");
        Text co = new Text("Login : " + this.getLogin() + " ");
        UtilisateurDAO uDAO = new UtilisateurDAO();
        Utilisateur utilisateur = uDAO.findWithLogin(this.getLogin());
        Menu listeFilm = new Menu("Liste Films");
        Menu ajouter_un_film = new Menu("Ajouter un Film");
        Menu titre = new Menu();
        titre.setText(co.getText());
        titre.setStyle("-fx-font: 18 arial;");
        ajouter_un_film.setStyle("-fx-font: 14 arial;");
        listeFilm.setStyle("-fx-font: 14 arial;");
        ajouter_un_film.getItems().addAll(item1);
        listeFilm.getItems().addAll(item2);
        this.setAlignment(deconnexion,Pos.BOTTOM_RIGHT);
        this.setBottom(deconnexion);
        this.setTop(menu);
        menu.getMenus().addAll(titre, ajouter_un_film, listeFilm);
        deconnexion.setOnMouseClicked(e -> {
            try {
                Main.scene.setEnabledPane(new LoginPane());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        item2.setOnAction( e-> {
            try {
                Main.scene.setEnabledPane(new ListeFilmPane());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        item1.setOnAction(e -> {
            try {
                Main.scene.setEnabledPane(new AjouterFilmPane());
            } catch (SQLException | ClassNotFoundException throwables) {
                throwables.printStackTrace();
            }
        });
        ajouter_un_film.setOnShowing(e -> item1.fire());
        listeFilm.setOnShowing(e-> item2.fire());
        InputStream n1 = getClass().getResourceAsStream("/icon/blockbuster.png");
        Image image1 = new Image(n1,500,300,false,true);
        BackgroundImage background = new BackgroundImage(image1, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        this.setBackground(new Background(background));


    }

    public static String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
