package Pane;

import DAO.FilmDAO;
import DAO.UtilisateurDAO;
import com.company.Film;
import com.company.Utilisateur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import com.company.Main;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Random;

public class AjouterFilmPane extends BorderPane {

    public AjouterFilmPane() throws SQLException, ClassNotFoundException {
        super();
        initPane();
    }




    public void initPane() throws SQLException, ClassNotFoundException {

        FilmDAO fDAO = new FilmDAO();


        Button retour = new Button("Retour");
        Button creer = new Button("Ajouter film");
        this.setAlignment(retour, Pos.TOP_LEFT);
        this.setAlignment(creer, Pos.BOTTOM_RIGHT);
        this.setBottom(creer);
        this.setTop(retour);
        GridPane champs = new GridPane();
        this.setCenter(champs);
        this.setAlignment(champs, Pos.CENTER);
        champs.setAlignment(Pos.CENTER);

        Alert filmCreer = new Alert(Alert.AlertType.INFORMATION);
        filmCreer.setContentText("film ajouté");

        Label titre = new Label("Titre :");
        Label dateSortie = new Label("Date de sortie :");
        TextField titreText = new TextField();
        TextField dateText = new TextField();

        Label numero = new Label("Numéro Episode :");
        Label recette = new Label("Recette :");
        TextField numeroText = new TextField();
        TextField recetteText = new TextField();
        Label cout = new Label("Cout :");
        TextField coutText = new TextField();

        Random random = new Random();
        int randInt = random.nextInt(99 -1 + 1) * 99;

        retour.setOnMouseClicked( e -> {
            try {
                Main.scene.setEnabledPane(new LoginPane());
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });

        champs.add(titre,0,0);
        champs.add(dateSortie,0,1);
        champs.add(titreText,1,0);
        champs.add(dateText,1,1);

        champs.add(numero, 0,2);
        champs.add(numeroText, 1,2);
        champs.add(recette,0,3);
        champs.add(recetteText,1,3);
        champs.add(cout,0,4);
        champs.add(coutText,1,4);


        champs.setHgap(20);
        champs.setVgap(20);



        creer.setOnMouseClicked( e -> {
            fDAO.create(new Film(randInt,
                    titreText.getText(),
                    Integer.parseInt(dateText.getText()),
                    Integer.parseInt(numeroText.getText()),
                    Integer.parseInt(coutText.getText()),
                    Integer.parseInt(recetteText.getText())));
            filmCreer.showAndWait();

        });
        retour.setOnMouseClicked(e -> {
            try {
                Main.scene.setEnabledPane(new AccueilPane(AccueilPane.getLogin()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });





    }

}

