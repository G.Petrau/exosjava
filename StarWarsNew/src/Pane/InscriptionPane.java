package Pane;

import com.company.Utilisateur;
import DAO.UtilisateurDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import com.company.Main;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

public class InscriptionPane extends BorderPane
{


    public InscriptionPane() throws SQLException, ClassNotFoundException {
        super();
        this.initPane();
    }



    public void initPane() throws SQLException, ClassNotFoundException {

        UtilisateurDAO utiDAO = new UtilisateurDAO();


        Button retour = new Button("Retour");
        Button creer = new Button("Créez votre compte");
        this.setAlignment(retour, Pos.TOP_LEFT);
        this.setAlignment(creer, Pos.BOTTOM_RIGHT);
        this.setBottom(creer);
        this.setTop(retour);
        GridPane champsInscription = new GridPane();
        this.setCenter(champsInscription);
        this.setAlignment(champsInscription, Pos.CENTER);
        champsInscription.setAlignment(Pos.CENTER);

        Alert utilisateurCreer = new Alert(Alert.AlertType.INFORMATION);
        utilisateurCreer.setContentText("Compte crée");

        Label login = new Label("Login :");
        Label mdp = new Label("Mot de passe :");
        TextField loginText = new TextField();
        PasswordField mdpText = new PasswordField();

        retour.setOnMouseClicked( e -> {
            try {
                Main.scene.setEnabledPane(new LoginPane());
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });

        champsInscription.add(login,0,0);
        champsInscription.add(mdp,0,1);
        champsInscription.add(loginText,1,0);
        champsInscription.add(mdpText,1,1);


        champsInscription.setHgap(20);
        champsInscription.setVgap(20);



        Random random = new Random();
        int randInt = random.nextInt(99 -1 + 1) * 99;

        creer.setOnMouseClicked( e -> {
            try {
                utiDAO.insert(randInt, loginText.getText(), mdpText.getText());
                Utilisateur utilisateur = utiDAO.findWithLogin(loginText.getText());
                utilisateurCreer.showAndWait();
            }
            catch (Exception ez)
            {
                ez.printStackTrace();
            }


        });



    }

}

