package Pane;

import DAO.FilmDAO;
import com.company.Film;
import com.company.Utilisateur;
import DAO.UtilisateurDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.PopupWindow;
import com.company.Main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class ListeFilmPane extends BorderPane
{

    public ListeFilmPane() throws SQLException, ClassNotFoundException {
        super();
        this.initPane();
    }


    public void initPane() throws SQLException, ClassNotFoundException {

        GridPane grid = new GridPane();
        this.setLeft(grid);
        FilmDAO fDAO = new FilmDAO();
        ArrayList<Film> liste = fDAO.findAll();
        Button retour = new Button("Retour");
        ArrayList<String> listeInfoFilm = new ArrayList<>();



        for(Film film : liste)
        {
            String info = film.getTitre() + "     " + film.getAnneeSortie();
            listeInfoFilm.add(info);
        }


        for(int i = 0; i < listeInfoFilm.size() ; i++)
        {
            MenuBar menuBar = new MenuBar();
            menuBar.setPrefWidth(300);
            Menu menu = new Menu(listeInfoFilm.get(i));
            MenuItem modifier = new MenuItem("modifier");
            MenuItem ajouterImage = new MenuItem("ajouter une image");
            int finalI = i;
            Film film = liste.get(finalI);
            if(film.getImage() != null)
            {
                Image image1 = new Image(film.getImage());
                ImageView img = new ImageView(image1);
                img.setFitHeight(50);
                img.setFitWidth(50);
                menu.setGraphic(img);
            }
            modifier.setOnAction( e-> {
                try
                {
                    Main.scene.setEnabledPane(new ModifierPane(liste.get(finalI).getId()));
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            });
            ajouterImage.setOnAction( e-> {
                FileChooser fileExplorer = new FileChooser();
                fileExplorer.setTitle("Ajouter une image");
                File file = fileExplorer.showOpenDialog(Main.scene.getWindow());
                film.setImage("file:///" + file.getAbsolutePath());
                try {
                    fDAO.ajouterImage(film);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                System.out.println(film.getImage());
                Image image1 = new Image(film.getImage());
                ImageView img = new ImageView(image1);
                img.setFitHeight(50);
                img.setFitWidth(50);
                menu.setGraphic(img);
            });



            menu.getItems().addAll(modifier, ajouterImage);
            menuBar.getMenus().add(menu);
            grid.add(menuBar, 0, i);

        }
        this.setTop(retour);
        retour.setOnMouseClicked(e -> {
            try {
                Main.scene.setEnabledPane(new AccueilPane(AccueilPane.getLogin()));
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });




    }

}

