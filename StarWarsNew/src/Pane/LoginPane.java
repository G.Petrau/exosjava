package Pane;

import com.company.Utilisateur;
import DAO.UtilisateurDAO;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import com.company.Main;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;

import java.io.InputStream;
import java.sql.SQLException;

public class LoginPane extends BorderPane {

    public LoginPane() throws SQLException, ClassNotFoundException {
        super();
        this.initPane();
    }

    private void initPane() throws SQLException, ClassNotFoundException {


        GridPane grid = initGrid();
        HBox region1 = new HBox();
        HBox region2 = new HBox();
        region1.setPrefWidth(220);
        region2.setPrefWidth(250);
        this.setAlignment(grid, Pos.CENTER);
        this.setCenter(grid);
        this.setLeft(region1);
        this.setRight(region2);
        InputStream n1 = getClass().getResourceAsStream("/icon/blockbuster.png");
        Image image1 = new Image(n1,150,100,false,true);
        BackgroundImage background = new BackgroundImage(image1, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        this.setBackground(new Background(background));


    }

    public GridPane initGrid() throws SQLException, ClassNotFoundException {
        GridPane champs = new GridPane();
        champs.setAlignment(Pos.CENTER);
        UtilisateurDAO utiDAO = new UtilisateurDAO();
        Label login = new Label("Login :");
        Label mdp = new Label("Mot de passe :");
        TextField loginText = new TextField();
        PasswordField mdpText = new PasswordField();
        Button boutonConnexion = new Button("Connexion");
        Button boutonInscription = new Button("Inscription");

        boutonConnexion.setShape(new Circle(3));
        boutonConnexion.setPrefSize(200,10);
        boutonInscription.setShape(new Circle(3));
        boutonInscription.setPrefSize(200,10);
        loginText.setPrefSize(200,10);
        mdpText.setPrefSize(200,10);

        champs.add(login,0,0);
        champs.add(mdp,0,1);
        champs.add(loginText,1,0);
        champs.add(mdpText,1,1);
        champs.add(boutonConnexion,1,2);
        champs.add(boutonInscription, 1, 3);
        champs.setVgap(20);
        champs.setHgap(20);

        boutonConnexion.setOnMouseClicked( e -> {
            try {
                Utilisateur utilisateur = utiDAO.CheckUser(loginText.getText(), mdpText.getText());
                if(utilisateur != null)
                {
                    Main.scene.setEnabledPane(new AccueilPane(utilisateur.getLogin()));
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
        boutonInscription.setOnMouseClicked( e -> {
            try {
                Main.scene.setEnabledPane(new InscriptionPane());
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });

        return champs;

    }



}

