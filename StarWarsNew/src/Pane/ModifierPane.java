package Pane;

import com.company.*;
import DAO.FilmDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import com.company.Main;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Random;

public class ModifierPane extends BorderPane
{
    int id;


    public ModifierPane(int id ) throws SQLException, ClassNotFoundException {
        super();
        this.id = id;
        this.initPane();
    }

    public void initPane() throws SQLException, ClassNotFoundException {

        FilmDAO fDAO = new FilmDAO();
        Film film = fDAO.find(id);

        Button retour = new Button("Retour");
        Button modifier = new Button("Mettre à jour film");
        this.setAlignment(retour, Pos.TOP_LEFT);
        this.setAlignment(modifier, Pos.BOTTOM_RIGHT);
        this.setBottom(modifier);
        this.setTop(retour);
        GridPane champs = new GridPane();
        this.setCenter(champs);
        this.setAlignment(champs, Pos.CENTER);
        champs.setAlignment(Pos.CENTER);

        Alert filmCreer = new Alert(Alert.AlertType.INFORMATION);
        filmCreer.setContentText("film modifié");

        Label titre = new Label("Titre :");
        Label dateSortie = new Label("Date de sortie :");
        TextField titreText = new TextField(film.getTitre());
        TextField dateText = new TextField(Integer.toString(film.getAnneeSortie()));

        Label numero = new Label("Numéro Episode :");
        Label recette = new Label("Recette :");
        TextField numeroText = new TextField(Integer.toString(film.getNumeroEpisode()));
        TextField recetteText = new TextField(Integer.toString(film.getRecette()));
        Label cout = new Label("Cout :");
        TextField coutText = new TextField(Integer.toString(film.getCout()));


        retour.setOnMouseClicked( e -> {
            try {
                Main.scene.setEnabledPane(new LoginPane());
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });

        champs.add(titre,0,0);
        champs.add(dateSortie,0,1);
        champs.add(titreText,1,0);
        champs.add(dateText,1,1);

        champs.add(numero, 0,2);
        champs.add(numeroText, 1,2);
        champs.add(recette,0,3);
        champs.add(recetteText,1,3);
        champs.add(cout,0,4);
        champs.add(coutText,1,4);


        champs.setHgap(20);
        champs.setVgap(20);



        modifier.setOnMouseClicked( e -> {

            fDAO.update(new Film(film.getId(),
                    titreText.getText(),
                    Integer.parseInt(dateText.getText()),
                    Integer.parseInt(numeroText.getText()),
                    Integer.parseInt(coutText.getText()),
                    Integer.parseInt(recetteText.getText())));
            filmCreer.showAndWait();

        });
        retour.setOnMouseClicked(e -> {
            try {
                Main.scene.setEnabledPane(new AccueilPane(AccueilPane.getLogin()));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }
}
