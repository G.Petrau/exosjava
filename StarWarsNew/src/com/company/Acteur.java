package com.company;

import java.util.HashMap;

public class Acteur {
    private int id;
    private String nom;
    private String prenom;
    private HashMap<String, Personnage> listePersonnages = new HashMap();

    public Acteur(int id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Acteur() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<String, Personnage> getListePersonnages() {
        return this.listePersonnages;
    }

    public void setListePersonnages(HashMap<String, Personnage> listePersonnages) {
        this.listePersonnages = listePersonnages;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int nbPersonnages() {
        return this.listePersonnages.size();
    }

    public String toString() {
        return "Acteur{nom='" + this.nom + "', prenom='" + this.prenom + "'}";
    }
}
