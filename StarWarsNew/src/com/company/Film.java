package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

public class Film {
    private int id;
    private String titre;
    private int anneeSortie;
    private int numeroEpisode;
    private int recette;
    private int cout;
    private ArrayList<Acteur> listeActeurs = new ArrayList();
    private String image;

    public Film(int id, String titre, int anneeSortie, int numeroEpisode, int recette, int cout) {
        this.id = id;
        this.titre = titre;
        this.anneeSortie = anneeSortie;
        this.numeroEpisode = numeroEpisode;
        this.recette = recette;
        this.cout = cout;
    }
    public Film(int id, String titre, int anneeSortie, int numeroEpisode, int recette, int cout, String image) {
        this.id = id;
        this.titre = titre;
        this.anneeSortie = anneeSortie;
        this.numeroEpisode = numeroEpisode;
        this.recette = recette;
        this.cout = cout;
        this.image = image;
    }

    public Film() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Acteur> getListeActeurs() {
        return this.listeActeurs;
    }

    public void setListeActeurs(ArrayList<Acteur> listeActeurs) {
        this.listeActeurs = listeActeurs;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getAnneeSortie() {
        return this.anneeSortie;
    }

    public void setAnneeSortie(int anneeSortie) {
        this.anneeSortie = anneeSortie;
    }

    public int getNumeroEpisode() {
        return this.numeroEpisode;
    }

    public void setNumeroEpisode(int numeroEpisode) {
        this.numeroEpisode = numeroEpisode;
    }

    public int getRecette() {
        return this.recette;
    }

    public void setRecette(int recette) {
        this.recette = recette;
    }

    public int getCout() {
        return this.cout;
    }

    public void setCout(int cout) {
        this.cout = cout;
    }

    public int nbActeurs() {
        return this.listeActeurs.size();
    }

    public int nbPersonnages() {
        int count = 0;
        Iterator var2 = this.listeActeurs.iterator();

        while(var2.hasNext()) {
            Acteur acteur = (Acteur)var2.next();
            if (acteur.getListePersonnages().get(this.getTitre()) != null) {
                ++count;
            }
        }

        return count;
    }

    public Object[] calculBenefice() {
        Object[] beneficiaire = new Object[]{this.getRecette() - this.getCout(), this.getRecette() - this.getCout() > 0};
        return beneficiaire;
    }

    public boolean isBefore(int annee) {
        return annee > this.anneeSortie;
    }

    public void tri() {
        this.getListeActeurs().sort(Comparator.comparing(Acteur::getNom).thenComparing(Acteur::getPrenom));
    }

    public String toString() {
        return "Film{id=" + this.id + ", titre='" + this.titre + "', anneeSortie=" + this.anneeSortie + ", numeroEpisode=" + this.numeroEpisode + ", recette=" + this.recette + ", cout=" + this.cout + ", listeActeurs=" + this.listeActeurs + "}";
    }
}
