package com.company;

import DAO.ActeurDAO;
import DAO.FilmDAO;
import Handler.SceneHandler;
import Pane.LoginPane;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Map.Entry;

public class Main extends Application {

    public static SceneHandler scene;

    @Override
    public void start(Stage primaryStage) throws Exception {

        try {
            LoginPane loginPane = new LoginPane();
            Rectangle2D screenbounds = Screen.getPrimary().getBounds();
            scene = new SceneHandler(loginPane,screenbounds.getWidth()*0.6, screenbounds.getHeight()*0.6);
            primaryStage.setTitle("Connexion");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (ClassNotFoundException e) {

            e.printStackTrace();
        }

    }

    public static void main(String[] args) {

        launch(args);
    }

    public static void parcoursFilms(ArrayList<Film> listeFilms) {
        Iterator var1 = listeFilms.iterator();

        while(var1.hasNext()) {
            Film film = (Film)var1.next();
            System.out.println(film.toString());
        }

    }

    public static void afficherBackup(HashMap<Integer, Film> dictionnaire) {
        Iterator var1 = dictionnaire.entrySet().iterator();

        while(var1.hasNext()) {
            Entry<Integer, Film> e = (Entry)var1.next();
            PrintStream var10000 = System.out;
            Object var10001 = e.getKey();
            var10000.println(var10001 + " - " + e.getValue() + " - " + ((Film)e.getValue()).calculBenefice()[0]);
        }

    }
}
