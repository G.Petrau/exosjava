package com.company;

import java.util.Random;

public class RandomGenerator {

    public static int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }

    public static String randDate()
    {
        Random r = new Random();
        int randDay = r.nextInt(7-1 + 1) + 4;
        int randMonth = r.nextInt(5-1+1)+5;
        int year = 2020;
        String date = year + "-" + "0" + randMonth + "-" +  "0" + randDay;
        System.out.println(date);
        return date;

    }
}

