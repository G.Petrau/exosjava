package com.company;

public class Utilisateur {

    private int id;
    private String login;
    private String mdp;

    public Utilisateur(int id, String login, String mdp) {
        this.id = id;
        this.login = login;
        this.mdp = mdp;
    }
    public Utilisateur(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "login='" + login + '\'' +
                ", mdp='" + mdp + '\'' +
                '}';
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
}
