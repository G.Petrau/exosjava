module StarWarsNew {
    requires java.sql;
    requires mysql.connector.java;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires tomcat.servlet.api;

    exports Pane;

    opens com.company;
}