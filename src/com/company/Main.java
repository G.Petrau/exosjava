package com.company;
import tools.*;


public class Main {


    public static void main(String[] args) {

        Visite v1 = new Visite();
        v1.setNom("baba");
        Visite v2 = new Visite("babu", "20/04/2021", 12);
        System.out.println(v1.getNom() + " , " + v2.getNom());
        MesDates dateFormatter = new MesDates();
        dateFormatter.usDateFormat(v2.getDate());
        Planning p1 = new Planning();
        p1.getPlanning().add(v1);
        p1.getPlanning().add(v2);
        p1.SupprVisite("baba");
        p1.getPlanning().forEach(visite -> System.out.println(visite.getNom()));



    }
}
