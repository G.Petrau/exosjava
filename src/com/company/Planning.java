package com.company;

import java.util.ArrayList;

public class Planning {

    private ArrayList<Visite> Planning = new ArrayList();

    public Planning(ArrayList<Visite> planning) {
        Planning = planning;
    }

    public Planning(){

    }

    public void AjoutVisite(Visite visite){
        this.getPlanning().add(visite);
    }

    public void SupprVisite(String etudiant)
    {
        this.getPlanning().removeIf(visite -> visite.getNom().equals(etudiant));
    }

    public ArrayList<Visite> getPlanning() {
        return Planning;
    }

    public void setPlanning(ArrayList<Visite> planning) {
        Planning = planning;
    }

}
