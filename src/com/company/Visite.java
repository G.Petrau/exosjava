package com.company;

import java.util.ArrayList;
import java.util.List;

public class Visite {

    private String nom;

    private String Date;

    private int Note;

    public Visite(String nom, String date, int note)
    {
        this.setNom(nom);
        this.setDate(date);
        this.setNote(note);
    }

    public Visite()
    {

    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getNote() {
        return Note;
    }

    public void setNote(int note) {
        Note = note;
    }

    @Override
    public String toString() {
        return "Visite{" +
                "nom='" + nom + '\'' +
                ", Date='" + Date + '\'' +
                ", Note=" + Note +
                '}';
    }


}
