package tools;

public class Calcul {

    int entier1;
    int entier2;

    public Calcul(int entier1, int entier2) {
        this.entier1 = entier1;
        this.entier2 = entier2;
    }

    public int getEntier1() {
        return entier1;
    }

    public void setEntier1(int entier1) {
        this.entier1 = entier1;
    }

    public int getEntier2() {
        return entier2;
    }

    public void setEntier2(int entier2) {
        this.entier2 = entier2;
    }

    public int addition(int nombre1, int nombre2)
    {
        return nombre1 + nombre2;
    }

    public int soustraction(int nombre1, int nombre2)
    {
        return nombre1 - nombre2;
    }

    public int multiplication(int nombre1, int nombre2)
    {
        return nombre1 * nombre2;
    }
    public int division(int nombre1, int nombre2)
    {
        try{
            return nombre1/nombre2;
        }
        catch (ArithmeticException e) {
            e.printStackTrace();
            throw e;
        }
    }

}
